﻿Imports UAI.TC.ProyectoEjemplo.Entities

Public Enum DVHCalculateStrategy
    JSON
    Reflexion
End Enum

Public Class IntegrityManager(Of T As Entity)
    Private Shared _strgy As DVHStrategy(Of T)

    Public Shared Function CheckIntegrity(entity As T, strgy As DVHCalculateStrategy) As Boolean

        setStrategy(strgy)
        If Not IsNothing(entity.DVH) Then
            If entity.DVH.Equals(_strgy.Calculate(entity)) Then
                Return True
            Else
                Throw New IntegrityException()
            End If
        Else
                Throw New IntegrityException()

            End If
        Return False
    End Function

    Public Shared Function CalculateDVH(entity As T, strgy As DVHCalculateStrategy) As String
        setStrategy(strgy)
        Return _strgy.Calculate(entity)
    End Function



    Private Shared Sub setStrategy(strgy As DVHCalculateStrategy)

        If strgy = DVHCalculateStrategy.Reflexion Then
            _strgy = New ReflexionStrategy(Of T)
        End If

    End Sub

End Class

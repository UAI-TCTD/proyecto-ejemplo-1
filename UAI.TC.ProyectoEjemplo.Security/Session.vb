﻿Imports UAI.TC.ProyectoEjemplo.Entities
Imports UAI.TC.ProyectoEjemplo.Security
Imports UAI.TC.ProyectoEjemplo.Services

Public Class Session
    Private _user As Usuario
    Public ReadOnly Property User As Usuario
        Get
            Return _user
        End Get
    End Property


    Public ReadOnly Property isLogged As Boolean
        Get
            Return Not IsNothing(User)
        End Get
    End Property

    Public Function Login(usuario As Usuario)
        _user = usuario
        SessionSubject.Notitfy(_user)
    End Function

    Public Sub Logout()
        _user = Nothing
        SessionSubject.Notitfy(_user)
    End Sub

End Class

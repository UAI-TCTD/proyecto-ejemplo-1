﻿Imports System.Reflection
Imports UAI.TC.ProyectoEjemplo.Entities
Imports UAI.TC.ProyectoEjemplo.Services

Public Class ReflexionStrategy(Of T As Entity)
    Inherits DVHStrategy(Of T)

    Public Overrides Function Calculate(entity As T) As String
        Dim x As Type = entity.GetType
        Dim value As String = vbNullString

        Dim props As PropertyInfo()
        Dim prop As PropertyInfo
        props = x.GetProperties()

        For Each prop In props
            If Not prop.Name.Equals("DVH") And Not prop.Name.Equals("Id") Then
                value = value & getString(prop.GetValue(entity))
            End If
        Next


        Dim hashed As String = CryptoManager.Hash(value, HashStrategy.SHA1)
        Return hashed


    End Function


    Private Function getString(val As Object) As String
        'definir estrategia para conversión de campos
        If TypeOf val Is Date Then
            Return Format(val, "DDMMYYYY")

        Else
            Return val.ToString
        End If

    End Function
End Class

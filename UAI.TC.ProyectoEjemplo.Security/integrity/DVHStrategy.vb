﻿Imports UAI.TC.ProyectoEjemplo.Entities

Public MustInherit Class DVHStrategy(Of T As Entity)
    MustOverride Function Calculate(entity As T) As String
End Class

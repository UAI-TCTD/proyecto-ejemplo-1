﻿Imports System.Security.Cryptography
Imports System.Text

Public MustInherit Class AbstractHashStrategy
    Friend hashAlgorithm As HashAlgorithm

    MustOverride Function Hash(input As String) As String

    Friend Function createString(value As Byte()) As String
        Dim sb As New StringBuilder()
        For index = 0 To value.Length - 1
            sb.Append(value(index).ToString("x2"))
        Next

        Return sb.ToString()

    End Function




End Class

﻿Imports System.Security.Cryptography
Imports System.Text

Public Class MD5Hash
    Inherits AbstractHashStrategy

    Public Overrides Function Hash(input As String) As String
        hashAlgorithm = MD5.Create()
        Dim data As Byte() = hashAlgorithm.ComputeHash(Encoding.UTF8.GetBytes(input))
        Return createString(data)
    End Function
End Class

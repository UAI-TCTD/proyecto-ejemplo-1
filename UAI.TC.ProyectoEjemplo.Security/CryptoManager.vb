﻿

Public Enum HashStrategy
    SHA1
    MD5
End Enum

Public Class CryptoManager



    Private Shared hashStrgy As AbstractHashStrategy
    Public Shared Function Hash(value As String, strgy As HashStrategy) As String
        setStrategy(strgy)

        Return hashStrgy.Hash(value)
    End Function
    Private Shared Sub setStrategy(strgy As HashStrategy)
        If strgy.Equals(HashStrategy.SHA1) Then
            hashStrgy = New SHA1Hash()
        ElseIf strgy.Equals(HashStrategy.MD5) Then
            hashStrgy = New MD5Hash()
        End If

    End Sub

    Public Shared Function Compare(input As String, hashedInput As String, strgy As HashStrategy) As Boolean
        setStrategy(strgy)
        Dim value = hashStrgy.Hash(input)
        Dim comparer As StringComparer = StringComparer.OrdinalIgnoreCase
        Return comparer.Compare(value, hashedInput).Equals(0)

    End Function








End Class

﻿Public Class LoginException
    Inherits Exception

    Public Property LoginResult As LoginResult

    Public Sub New(result As LoginResult)
        LoginResult = result
    End Sub


End Class

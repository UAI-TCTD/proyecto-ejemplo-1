﻿Imports UAI.TC.ProyectoEjemplo.Entities

Public MustInherit Class AbstractSubject(Of T As Entity)


    Private Shared observers As New ArrayList()
    Public Shared Sub AddObsever(observer As IObserver(Of T))
        observers.Add(observer)
    End Sub

    Public Shared Sub RemoveObsever(observer As IObserver(Of T))
        observers.Remove(observer)
    End Sub


    Public Shared Sub Notitfy(entity As T)
        For Each obs As IObserver(Of T) In observers
            obs.Notify(entity)
        Next
    End Sub

End Class

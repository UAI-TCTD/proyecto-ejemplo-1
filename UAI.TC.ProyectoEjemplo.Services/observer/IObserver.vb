﻿Imports UAI.TC.ProyectoEjemplo.Entities

Public Interface IObserver(Of T As Entity)
    Sub Notify(entity As T)
End Interface

﻿
Public Enum SerializationStrategy
    JSON
End Enum
Public Class Serializator(Of T)
    Shared _strgy As AbstractSerializator
    Public Shared Function Serialize(entity As T, strgy As SerializationStrategy)
        If strgy = SerializationStrategy.JSON Then
            _strgy = New JsonSerializator(Of T)
        End If

        Return _strgy.Serializar(entity)
    End Function
End Class

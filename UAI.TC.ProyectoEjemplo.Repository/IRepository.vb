﻿Public Interface IRepository(Of T As Class)

    'Proporciona manejo de las actividades sobre la BD como
    'commit, begin transaction, rolling back a transaction, etc.
    'Property DbContext As IDbContext

    'Devuelve nulo si no se encontró coincidencia con el ID
    Function Obtain(id_ As Object) As T
    Function SaveOrUpdate(entidad_ As T) As T
    Sub Delete(entidad_ As T)
    Function GetAll() As IQueryable(Of T)

End Interface

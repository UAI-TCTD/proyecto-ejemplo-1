﻿Imports UAI.TC.ProyectoEjemplo.DAL
Imports UAI.TC.ProyectoEjemplo.Entities
Imports UAI.TC.ProyectoEjemplo.Repository

Public Interface IUsuarioRepository
    Inherits IRepository(Of Usuario)

    Function GetByUsernamePassword(username As String) As Usuario

End Interface

Public Class UsuarioRepository
    Inherits RepositoryBase(Of Usuario)
    Implements IUsuarioRepository



    Sub New()
        MyBase.New(New UsuarioContext())
    End Sub

    Public Function GetByUsernamePassword(username As String) As Usuario Implements IUsuarioRepository.GetByUsernamePassword
        Return _context.GetAll("username='" & username & "'").FirstOrDefault()
    End Function
End Class
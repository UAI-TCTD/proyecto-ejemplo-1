﻿Imports UAI.TC.ProyectoEjemplo.DAL
Imports UAI.TC.ProyectoEjemplo.Entities
Imports UAI.TC.ProyectoEjemplo.Repository
Imports UAI.TC.ProyectoEjemplo.Security

Public MustInherit Class RepositoryBase(Of T As Entity)
    Implements IRepository(Of T)

    Protected _context As IDbContext(Of T)

    Sub New(context As IDbContext(Of T))
        _context = context
    End Sub

    Public Sub Delete(entidad_ As T) Implements IRepository(Of T).Delete
        _context.Delete(entidad_.Id)
    End Sub

    Public Function GetAll() As IQueryable(Of T) Implements IRepository(Of T).GetAll
        Return _context.GetAll()
    End Function

    Public Function Obtain(id_ As Object) As T Implements IRepository(Of T).Obtain
        Return _context.GetById(id_)
    End Function

    Public Function SaveOrUpdate(entidad_ As T) As T Implements IRepository(Of T).SaveOrUpdate

        entidad_.DVH = IntegrityManager(Of T).CalculateDVH(entidad_, DVHCalculateStrategy.Reflexion)
        Return _context.Save(entidad_)
    End Function
End Class


﻿Imports UAI.TC.ProyectoEjemplo.DAL
Imports UAI.TC.ProyectoEjemplo.Entities

Public Interface IClienteRepository
    Inherits IRepository(Of Cliente)


End Interface
Public Class ClienteRepository
    Inherits RepositoryBase(Of Cliente)
    Implements IClienteRepository



    Sub New()
        MyBase.New(New ClienteContext())
    End Sub


End Class

﻿Imports UAI.TC.ProyectoEjemplo.Repository

Public MustInherit Class BaseBLL
    Public Class BaseBLL(Of T As Class)
        Implements IBLL(Of T)

        Protected Repository As IRepository(Of T)


        Public Sub Delete(id As Object) Implements IBLL(Of T).Delete
            Dim entity As T
            entity = Repository.Obtain(id)
            Repository.Delete(entity)
        End Sub

        Public Function Obtain(id As Object) As T Implements IBLL(Of T).Obtain
            Return Repository.Obtain(id)
        End Function

        Public Sub OnAfterSaveOrUpdate(entity As T) Implements IBLL(Of T).OnAfterSaveOrUpdate

        End Sub

        Public Sub OnBeforeDelete(entity As T) Implements IBLL(Of T).OnBeforeDelete

        End Sub

        Public Sub OnBeforeSaveOrUpdate(entity As T) Implements IBLL(Of T).OnBeforeSaveOrUpdate


        End Sub

        Public Overridable Function SaveOrUpdate(Entity As T) As T Implements IBLL(Of T).SaveOrUpdate
            Return Repository.SaveOrUpdate(Entity)

        End Function

        Public Function GetAll() As IQueryable(Of T) Implements IBLL(Of T).GetAll
            Return Repository.GetAll()
        End Function


    End Class

End Class

﻿
Imports UAI.TC.ProyectoEjemplo.BLL.BaseBLL
Imports UAI.TC.ProyectoEjemplo.Entities
Imports UAI.TC.ProyectoEjemplo.Repository
Imports UAI.TC.ProyectoEjemplo.Security

Public Interface IUsuarioBLL
    Function Login(username As String, password As String) As LoginResult
End Interface

Public Class UsuarioBLL
    Inherits BaseBLL(Of Usuario)
    Implements IUsuarioBLL

    Dim repo As IUsuarioRepository

    Sub New()

        repo = New UsuarioRepository
        MyBase.Repository = repo


    End Sub



    Public Function Login(username As String, password As String) As LoginResult Implements IUsuarioBLL.Login
        Dim res As LoginResult
        Dim usuario As Usuario

        usuario = repo.GetByUsernamePassword(username)
        If IsNothing(usuario) Then
            Throw New LoginException(LoginResult.InvalidUsername)
        Else
            If CryptoManager.Compare(password, usuario.Password, HashStrategy.MD5) Then
                SessionManager.Instance.Login(usuario)
                Return LoginResult.ValidUser
            Else
                Throw New LoginException(LoginResult.InvalidPassword)
            End If
        End If


        Return res

    End Function
End Class

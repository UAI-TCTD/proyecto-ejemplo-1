﻿Imports UAI.TC.ProyectoEjemplo.Entities
Imports UAI.TC.ProyectoEjemplo.Security

Public Class UsuarioMapper
    Inherits Mapper


    Public Overrides Function Map(row As DataRow) As Object

        Dim p As New Usuario

        p.Id = row("id")
        p.DVH = row("DVH")
        p.Username = row("username")
        p.Password = row("password")

        IntegrityManager(Of Usuario).CheckIntegrity(p, DVHCalculateStrategy.Reflexion)

        Return p
    End Function
End Class

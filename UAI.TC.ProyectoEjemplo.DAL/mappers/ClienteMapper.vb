﻿Imports UAI.TC.ProyectoEjemplo.Entities
Imports UAI.TC.ProyectoEjemplo.Security

Public Class ClienteMapper
    Inherits Mapper


    Public Overrides Function Map(row As DataRow) As Object

        Dim p As New Cliente

        p.Id = row("id")
        p.DVH = row("DVH")
        p.Apellido = row("username")
        p.Nombre = row("password")

        IntegrityManager(Of Cliente).CheckIntegrity(p, DVHCalculateStrategy.Reflexion)

        Return p
    End Function
End Class

﻿Public Interface IUnitOfWork
    Sub CommitTransaction()
    Sub RollBackTransaction()
    Function CreateCommand() As IDbCommand
End Interface


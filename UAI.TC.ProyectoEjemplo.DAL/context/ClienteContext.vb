﻿Imports System.Data.SqlClient
Imports UAI.TC.ProyectoEjemplo.Entities

Public Class ClienteContext
    Inherits AbstractContext(Of Cliente)

    Public Overrides Function Delete(id As Long) As Boolean
        Throw New NotImplementedException()
    End Function

    Public Overrides Function GetAll(filter As String) As IQueryable(Of Cliente)

        _cmd = _uow.CreateCommand()
        _cmd.CommandText = "SELECT * FROM clientes WHERE 1=1  AND " & filter

        Dim lista As New List(Of Cliente)
        Dim data As SqlDataAdapter = New SqlDataAdapter(_cmd) '
        Dim dset As DataSet = New DataSet()
        data.Fill(dset, "clientes")



        Dim p As New Cliente

        For Each usuario As DataRow In dset.Tables("clientes").Rows
            p = MapperFactory.Instance.MapperFactory(Of Cliente)().Map(usuario)


            If Not lista.Exists(Function(c) c.Id = p.Id) Then
                lista.Add(p)
            End If

        Next
        Return lista.AsQueryable
    End Function

    Public Overrides Function Save(Entity As Cliente) As Object

        _cmd = _uow.CreateCommand()

        _cmd.Parameters.Add(New SqlParameter("apellido", Entity.Apellido))
        _cmd.Parameters.Add(New SqlParameter("dvh", Entity.DVH))
        _cmd.Parameters.Add(New SqlParameter("nombre", Entity.Nombre))
        If Entity.Id.Equals(0) Then

            _cmd.CommandText = "INSERT INTO clientes (apellido,nombre,DVH) values (@apellido, @nombre,@dvh);SELECT scope_identity()"
            Entity.Id = _cmd.ExecuteScalar()

        Else
            _cmd.Parameters.Add(New SqlParameter("id", Entity.Id))
            _cmd.CommandText = "UPDATE clientes SET DVH=@dvh, apellido = @apellido, nombre= @nombre WHERE idCliente=@Id"
            _cmd.ExecuteNonQuery()
        End If





        _cmd.Transaction.Commit()

        Return Entity
    End Function
End Class

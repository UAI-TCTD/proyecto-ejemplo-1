﻿
Imports System.Data.SqlClient
Imports UAI.TC.ProyectoEjemplo.Entities

Public Class UsuarioContext
    Inherits AbstractContext(Of Usuario)

    Public Overrides Function Save(Entity As Usuario) As Object



        _cmd = _uow.CreateCommand()

        _cmd.Parameters.Add(New SqlParameter("username", Entity.Username))
        _cmd.Parameters.Add(New SqlParameter("dvh", Entity.DVH))
        _cmd.Parameters.Add(New SqlParameter("password", Entity.Password))
        If Entity.Id.Equals(0) Then

            _cmd.CommandText = "INSERT INTO usuarios (username,password,DVH) values (@username, @password,@dvh);SELECT scope_identity()"
            Entity.Id = _cmd.ExecuteScalar()

        Else
            _cmd.Parameters.Add(New SqlParameter("id", Entity.Id))
            _cmd.CommandText = "UPDATE Usuario SET DVH=@dvh, username = @username, password= @password WHERE idUsuario=@Id"
            _cmd.ExecuteNonQuery()
        End If





        _cmd.Transaction.Commit()

        Return Entity
    End Function



    Public Overrides Function GetAll(filter As String) As IQueryable(Of Usuario)

        _cmd = _uow.CreateCommand()
        _cmd.CommandText = "SELECT * FROM usuarios WHERE 1=1  AND " & filter

        Dim lista As New List(Of Usuario)
        Dim data As SqlDataAdapter = New SqlDataAdapter(_cmd) '
        Dim dset As DataSet = New DataSet()
        data.Fill(dset, "usuarios")



        Dim p As New Usuario

        For Each usuario As DataRow In dset.Tables("usuarios").Rows
            p = MapperFactory.Instance.MapperFactory(Of Usuario)().Map(usuario)


            If Not lista.Exists(Function(c) c.Id = p.Id) Then
                lista.Add(p)
            End If

        Next
        Return lista.AsQueryable
    End Function



    Public Overrides Function Delete(id As Long) As Boolean
        Throw New NotImplementedException()
    End Function
End Class

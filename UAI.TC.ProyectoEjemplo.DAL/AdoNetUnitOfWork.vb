﻿Imports System.Configuration
Imports System.Data.SqlClient

Public Class AdoNetUnitOfWork
    Implements IUnitOfWork


    Private _connection As IDbConnection
    Private ReadOnly _lock As Object = New Object()
    Private _transaction As SqlTransaction


    Sub New()





        _connection = New SqlConnection(ConfigurationManager.ConnectionStrings("UaiTC_ConnectionString").ConnectionString)
        _connection.Open()

        _transaction = _connection.BeginTransaction
    End Sub

    Public Sub CommitTransaction() Implements IUnitOfWork.CommitTransaction
        _transaction.Commit()
    End Sub

    Public Sub RollBackTransaction() Implements IUnitOfWork.RollBackTransaction
        _transaction.Rollback()
    End Sub

    Public Function CreateCommand() As IDbCommand Implements IUnitOfWork.CreateCommand
        Dim _command As IDbCommand = _connection.CreateCommand()
        _command.Transaction = _transaction
        Return _command
    End Function




End Class

﻿Imports UAI.TC.ProyectoEjemplo.BLL
Imports UAI.TC.ProyectoEjemplo.Entities
Imports UAI.TC.ProyectoEjemplo.Security
Imports UAI.TC.ProyectoEjemplo.Services

Public Class frmLogin
    Dim bll As New UsuarioBLL
    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles cmdLogin.Click
        Try

            If bll.Login(Me.txtUsername.Text, Me.txtPassword.Text) = LoginResult.ValidUser Then

                Me.Close()
            End If
        Catch ex As LoginException
            MsgBox(ex.LoginResult.ToString, MsgBoxStyle.Critical, "Error")
        Catch ex2 As IntegrityException
            MsgBox(ex2.Message, MsgBoxStyle.Critical, "Error")
            SessionManager.Instance.Logout()
        Catch ex2 As Exception
            'handle exception
            MsgBox(ex2.Message, MsgBoxStyle.Critical, "Error")
        End Try

    End Sub

    Private Sub frmLogin_Load(sender As Object, e As EventArgs) Handles MyBase.Load

    End Sub

    Private Sub validate()
        Me.cmdLogin.Enabled = (Me.txtPassword.Text.Length > 0 And Me.txtUsername.Text.Length > 0)
    End Sub

    Private Sub txtUsername_TextChanged(sender As Object, e As EventArgs) Handles txtUsername.TextChanged
        validate()
    End Sub

    Private Sub txtPassword_TextChanged(sender As Object, e As EventArgs) Handles txtPassword.TextChanged
        validate()
    End Sub
End Class

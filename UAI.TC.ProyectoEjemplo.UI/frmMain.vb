﻿Imports System.Windows.Forms
Imports UAI.TC.ProyectoEjemplo.BLL
Imports UAI.TC.ProyectoEjemplo.Entities
Imports UAI.TC.ProyectoEjemplo.Security
Imports UAI.TC.ProyectoEjemplo.Services

Public Class frmMain
    Implements IUsuarioObserver


    Public Sub SetPermission()

        Me.mnuIniciarSesion.Enabled = Not SessionManager.Instance.isLogged
        Me.mnuCerrarSesion.Enabled = SessionManager.Instance.isLogged
        If Not SessionManager.Instance.isLogged Then
            Me.toolSessionName.Text = "Inicie Sesión"
        Else
            Me.toolSessionName.Text = "Bienvenido " & SessionManager.Instance.User.Username
        End If


    End Sub

    Private Sub mnuIniciarSesion_Click(sender As Object, e As EventArgs) Handles mnuIniciarSesion.Click
        Dim frm As New frmLogin
        frm.MdiParent = Me
        frm.Show()

    End Sub

    Private Sub frmMain_Activated(sender As Object, e As EventArgs) Handles MyBase.Activated
        SetPermission()
    End Sub

    Private Sub frmMain_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        SessionSubject.AddObsever(Me)
        Dim bll As New UsuarioBLL
        Dim u As Usuario = bll.GetAll().Where(Function(p) p.Username.Equals("admin")).FirstOrDefault
        If IsNothing(u) Then
            u = New Usuario()
            u.Username = "admin"
            u.Password = CryptoManager.Hash("admin", HashStrategy.MD5)
            bll.SaveOrUpdate(u)
        End If

        Dim cbll = New ClienteBLL
        Dim c As New Cliente
        c.Apellido = "perez"
        c.Nombre = "juan"
        cbll.SaveOrUpdate(c)
    End Sub

    Public Sub Notify(entity As Usuario) Implements IObserver(Of Usuario).Notify
        SetPermission()
    End Sub

    Private Sub frmMain_FormClosing(sender As Object, e As FormClosingEventArgs) Handles MyBase.FormClosing
        SessionSubject.RemoveObsever(Me)
    End Sub

    Private Sub mnuCerrarSesion_Click(sender As Object, e As EventArgs) Handles mnuCerrarSesion.Click
        SessionManager.Instance.Logout()
    End Sub
End Class
